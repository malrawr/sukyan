# Sukyan (スキャン)

A penetration testing script that automatically performs vulnerability scanning and auditing against a target. Features various improvements over PenEnum such as a configurable json file in which commands are pulled from as well as quicker scanning.

## To-Do
* Update JSON Config
  * Gather better commands to use
* Integrate Quickfire
* Add a quiet mode
  * Basically if you use the `-v0` flags then it won't diplay NMAP or AMAP output to STDOUT
* Add AMAP function for UDP
* Remove ruby colorize dependency
* Fix color output
* Refactor
* Add credits (found inside old penenum.sh)

### Mini Script Version

Basically a small version of the script that takes an IP and SERVICES as argument.
For example, `script.rb -t 10.10.10.10 -s http,https,mysql` 

Then it reads in the config file for each service, if it finds it then you are prompted for a port number.
It'll then display the results back to you. It's basically like the full sukyan except without the initial scan. It just parses the json file

### Miniature Sukyan for automatic HTTP/HTTPS

Create a smaller version specifically for doing CTFs quickly and not for general purpose.
