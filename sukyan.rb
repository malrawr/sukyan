#!/usr/bin/ruby

require 'fileutils'
require 'erb'
require 'colorize'
require 'json'

# Helper methods are defined here
#module TerminalColor    
    RS = "\033[0m"    # RESET
    HC = "\033[1m"    # Strong BOLD
    FBLU = "\033[34m" # Foreground Blue
    FRED = "\033[31m" # Foreground Red
    FGRN = "\033[32m" # Foreground Green
    FWHT = "\033[37m" # Foreground White

    def print_stat(string)
        puts "#{FBLU}#{HC}[*]#{HC}#{FBLU}" << print_reset << " " << string
    end

    def print_warn(string)
        puts "#{FRED}#{HC}[x]#{HC}#{FRED}" << print_reset << " " << string
    end

    def print_sugg(string)
        puts "   #{FGRN}#{HC}[>]#{HC}#{FGRN}" << print_reset << " " << string
    end

    def print_exec(string)
        GLOBAL_ARRAY << string
        puts "      #{FWHT}#{HC}[#{FGRN}#{GLOBAL_ARRAY.index(string)}#{FGRN}#{FWHT}]#{HC}#{FWHT}" << print_reset << " " << string
    end

    def print_norm(string)
        puts "      #{FWHT}#{HC}[=]#{HC}#{FWHT}" << print_reset << " " << string
    end

    def print_exit(string)
        puts ""
        puts "#{FRED}" << string << "#{FRED}" << print_reset
        puts ""
    end

    def print_reset
        return "#{RS}"
    end

    def prompt(string, warn=false)
        if warn
            print "\n#{FRED}#{HC}>>#{FRED}#{HC}" << " " << print_reset <<  string << "\n" << "#{HC}>?#{HC}" << print_reset << " "
        else
            print "\n#{FGRN}#{HC}>>#{FGRN}#{HC}" << " " << print_reset <<  string << "\n" << "#{HC}>?#{HC}" << print_reset << " " 
        end
    end

    def space
        puts ""
    end

#end
# End helper methods

def nmap_tcp_exec(fulltest = false)
    print_stat "Running NMAP on #{IP} to find OPEN Ports and Services"

    if fulltest
        print_sugg "Executing Full TCP Scan"
        print_norm "nmap -sC -sV -T 4 #{IP} -p- -oG #{FILE_OUTPUT}/greplist_#{IP}.nmap -oN #{FILE_OUTPUT}/tcp_full_#{IP}.nmap"
        system("bash", "-c", "nmap -sC -sV -T 4  #{IP} -p- -oG #{FILE_OUTPUT}/greplist_#{IP}.nmap -oN #{FILE_OUTPUT}/tcp_full_#{IP}.nmap")
        space
    else
        print_sugg "Executing Standard TCP Scan"
        print_norm "nmap -sC -sV #{IP} -oG #{FILE_OUTPUT}/greplist_#{IP}.nmap -oN #{FILE_OUTPUT}/tcp_standard_#{IP}.nmap"
        system("bash", "-c", "nmap -sC -sV #{IP} -oG #{FILE_OUTPUT}/greplist_#{IP}.nmap -oN #{FILE_OUTPUT}/tcp_standard_#{IP}.nmap")
        space
    end
end

def nmap_udp_exec
    print_sugg "Executing Top 200 UDP Scan"
    print_norm "nmap -sC -sV -sU -T 4 #{IP} --top-ports 200 -oN #{FILE_OUTPUT}/udp_top200_#{IP}.nmap"
    Process.fork { system("bash", "-c", "nmap -sC -sV -sU -T 4 #{IP} --top-ports 200 -oN #{FILE_OUTPUT}/udp_top200_#{IP}.nmap", :out => File::NULL) }
end

def amap_tcp
  system("rm #{FILE_OUTPUT}/amap_scan_tcp_#{IP}", :out => File::NULL, :err => File::NULL) #Delete old file so that it doesn't get appended like crazy
  amap_ports = `cat #{FILE_OUTPUT}/ports_#{IP}.list`.split("\n")
  unless amap_ports.nil? || amap_ports.empty?
    print_stat "Running AMAP on #{IP} for a more detailed enumeration"
    print_sugg "Be sure to review the output file at #{FILE_OUTPUT}/amap_scan_#{IP}"
    amap_ports.each do |x|
      print_norm "amap -Adbqv -1 -b #{ARGS[:target]} #{x}".magenta
      unless ARGS[:show_amap] 
        system("amap -Adbqv -1 -b #{ARGS[:target]} #{x} | tail -n +9 | sed '$d' | tee -a #{FILE_OUTPUT}/amap_scan_tcp_#{IP}", :out => File::NULL)
      else
        system("amap -Adbqv -1 -b #{ARGS[:target]} #{x} | tail -n +9 | sed '$d' | tee -a #{FILE_OUTPUT}/amap_scan_tcp_#{IP}")
      end
    end
  end
end

def parse_nmap
    # Go back and fix this line below
    #test_line = IO.readlines("grep.config")[4].gsub(/\s+/, "").tr(',', "\n").sub(/Ignored.*/mi, "").gsub(/.*:/, '')
    `cat #{FILE_OUTPUT}/greplist_#{IP}.nmap | grep "Ports:" | sed 's/Ignored.*//' | cut -d " " -f4- | tr "," "\n" | tr -d " " | tee #{FILE_OUTPUT}/parsed_#{IP}.list &> /dev/null`
    `cat #{FILE_OUTPUT}/parsed_#{IP}.list | cut -d "/" -f1 | tee #{FILE_OUTPUT}/ports_#{IP}.list &> /dev/null`
    `cat #{FILE_OUTPUT}/parsed_#{IP}.list | cut -d "/" -f5 | tee #{FILE_OUTPUT}/services_#{IP}.list &> /dev/null`
end

def port_list
    port_list_path = File.join(FILE_OUTPUT, "ports_#{IP}.list")
    port_list_file = File.open(port_list_path, 'r').readlines
    port_list_file.map! { |x| x.chomp }
    return port_list_file
end

def service_list
    service_list_path = File.join(FILE_OUTPUT, "services_#{IP}.list")
    service_list_file = File.open(service_list_path, 'r').readlines
    service_list_file.map! { |x| x.chomp }    
    return service_list_file
end

def command_suggestions(ports, services)
  file = IO.read('config-sukyan.json')
  data = JSON.parse(file)

  found_services = []
  unknown_services = []

  ports.zip(services) do | port, service |
    data.each do |key, value|
      if value["nmap-service-names"].include?(service)
        print_stat "#{value["description"]}".blue % {:IP => IP, :PORT => port, :FILE_OUTPUT => FILE_OUTPUT}
        value["output"].each do |a|
            if a["file"] 
              a["file"].each do |x|
                print_sugg "#{x["description"]}".light_red % {:IP => IP, :PORT => port, :FILE_OUTPUT => FILE_OUTPUT}
                x["commands"].each do |y|
                  print_exec y.to_s % {:IP => IP, :PORT => port, :FILE_OUTPUT => FILE_OUTPUT}
                end
              end
            end
            if a["other"]
              a["other"].each do |x|
                print_sugg "#{x["description"]}".light_red % {:IP => IP, :PORT => port, :FILE_OUTPUT => FILE_OUTPUT}
                x["commands"].each do |y|
                  print_norm y.to_s % {:IP => IP, :PORT => port, :FILE_OUTPUT => FILE_OUTPUT}
                end
              end
            end
        end
        found_services << service
      end
    end
    unless found_services.include?(service)
      unknown_services << "#{service}/#{port}"
    end
  end
  
  print_stat "Finished enumerating these services: #{found_services}".yellow 
  
  unless unknown_services.empty?
    print_warn "Found the following services: #{unknown_services}"
    print_sugg "These services could not not be enumerated, check out the AMAP output and update the JSON config file"
    print_norm "#{FILE_OUTPUT}/amap_scan_#{IP}"
  end 
end

def command_execution
    # Make sure that people can't put in a value larger than array.length
    print_stat("What would you like to run? There are #{GLOBAL_ARRAY.length} commands available. Add a space next to each selection.")
    print_stat("Example Selection: 2 1 7")
    value = gets.chomp.split(' ').map(&:to_i)
    value.each do | x |
        if x < GLOBAL_ARRAY.length
            #Process.fork { system("#{GLOBAL_ARRAY[x]}", "&") }
            Process.fork { system("bash", "-c", "#{GLOBAL_ARRAY[x]}", :out => File::NULL) }
        else
            print_warn "An invalid number was included, ommiting #{GLOBAL_ARRAY[x]}..."
        end
    end
    print_stat("All commands should be running now. Good luck! You can find that results at: #{FILE_OUTPUT}")
end


def display_all_suggestions
# This method is for testing purposes.
# It outputs the whole json config in a custom format

  test_ip = "10.10.10.10"
  test_output = "/tmp/"

  file = IO.read("#{File.expand_path(File.dirname(__FILE__))}/config-sukyan.json")
  data = JSON.parse(file)

  data.each do |key, value|
    test_port = rand(0...65535).to_s # Generates a different port number so that there are no array element duplicates.
    print_stat "Description: #{value["description"]}" % {:IP => test_ip, :PORT => test_port, :FILE_OUTPUT => test_output}
    value["output"].each do |a| 
      if a["file"]
               a["file"].each do |x|
                 print_sugg "Description: #{x["description"]}" % {:IP => test_ip, :PORT => test_port, :FILE_OUTPUT => test_output}
                 x["commands"].each do |y|
                   print_exec y.to_s % {:IP => test_ip, :PORT => test_port, :FILE_OUTPUT => test_output}
                 end
               end
             end
             if a["other"]
               a["other"].each do |x|
                 print_sugg "Description: #{x["description"]}" % {:IP => test_ip, :PORT => test_port, :FILE_OUTPUT => test_output}
                 x["commands"].each do |y|
                   print_norm y.to_s % {:IP => test_ip, :PORT => test_port, :FILE_OUTPUT => test_output}
                 end
               end
             end

    end 
  end 
end

VERSION = "0.1"

BANNER = <<ENDBANNER
███████╗██╗   ██╗██╗  ██╗██╗   ██╗ █████╗ ███╗   ██╗
██╔════╝██║   ██║██║ ██╔╝╚██╗ ██╔╝██╔══██╗████╗  ██║
███████╗██║   ██║█████╔╝  ╚████╔╝ ███████║██╔██╗ ██║
╚════██║██║   ██║██╔═██╗   ╚██╔╝  ██╔══██║██║╚██╗██║
███████║╚██████╔╝██║  ██╗   ██║   ██║  ██║██║ ╚████║
╚══════╝ ╚═════╝ ╚═╝  ╚═╝   ╚═╝   ╚═╝  ╚═╝╚═╝  ╚═══╝
ENDBANNER

USAGE = <<ENDUSAGE
Usage: #{$0} -t target [-u] [-a] [-h] [-v]
ENDUSAGE

HELP = <<ENDHELP
  -b, --banner     Show #{$0} banner
  -h, --help       Show this help.
  -t, --target     Target IP Address. This option is required.
  -u, --udp        Scan Top 200 UDP Ports
  -a, --all-tcp    Scans all TCP Ports
  -j, --json       Tests JSON Output
  -s, --show-amap  Shows AMAP Output
ENDHELP

ARGS = {  } # Setting default values
UNFLAGGED_ARGS = []
next_arg = UNFLAGGED_ARGS.first
ARGV.each do |arg|
  case arg
    when '-b','--banner'    then ARGS[:banner]    = true
    when '-h','--help'      then ARGS[:help]      = true
    when '-t','--target'    then next_arg = :target
    when '-u','--udp'       then ARGS[:udp]       = true
    when '-a','--all-tcp'   then ARGS[:all_tcp]   = true
    when '-j','--json'      then ARGS[:json]      = true
    when '-s','--show-amap' then ARGS[:show_amap] = true
    else
      if next_arg
        ARGS[next_arg] = arg
        UNFLAGGED_ARGS.delete( next_arg )
      end
      next_arg = UNFLAGGED_ARGS.first
    end
end

puts "#{$0} v#{VERSION}"

if ARGS[:banner]
  puts BANNER
end

if ARGS[:help] or !ARGS[:target]
  puts USAGE unless ARGS[:version]
  puts HELP if ARGS[:help]
  exit
end

trap("SIGINT") do
    print_exit "WARNING! CTRL + C Detected, Shutting things down and exiting program...."
    exit
end

unless ARGS[:target] 
  raise("#{$0}: No target was specified")
end

CURRENT_DIRECTORY = File.dirname(__FILE__)
GLOBAL_ARRAY = []
IP = "#{ARGS[:target]}"

puts "Initializing scan against #{IP}"

FILE_OUTPUT = "#{CURRENT_DIRECTORY}" << "/results/" << "#{IP}"

print_stat "Preparing output directories, #{FILE_OUTPUT}"

FileUtils.mkdir_p "#{FILE_OUTPUT}"

if ARGS[:json] then display_all_suggestions; exit end

if ARGS[:all_tcp]
    puts "Running Full TCP Scan"
    nmap_tcp_exec(true)
else
    nmap_tcp_exec
end

if ARGS[:udp]
    puts "Running Top 200 UDP"
    nmap_udp_exec
end

parse_nmap
amap_tcp
command_suggestions(port_list, service_list)

ARGV.clear

prompt "Would you like to run any commands? y/n"
while user_input = gets.chomp.downcase
    case user_input
    when "y"
        command_execution
        break
    when "n"
        print_stat("Good luck!")
        break
    else
        prompt "Select a correct response.", true
    end
end
